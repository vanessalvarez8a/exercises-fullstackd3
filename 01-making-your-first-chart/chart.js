async function drawLineChart() {

  //---- 1. Access Data
  // await will pause the execution of a function until la promise is resolved
  // This will only work within async function in this case drawLineChart()
  const dataset = await d3.json('./my_weather_data.json');
  console.table(dataset[0]);  // dataset is a table

  // NOTE PAGE 10: is important to do Accessor functions for easy changes, documentation, framing
  // yAccessor for plotting points on the y axis 
  const yAccessor = d => d.temperatureMax; 

  // d3.timeParse() takes a string a date format and outputs a function that will parse dates of that format
  const dateParser = d3.timeParse('%Y-%m-%d');

  // xAccessor plotting points on x axis
  const xAccessor = d => dateParser(d.date);


  //---- 2. Create Chart Dimensions
  // Defining dimensions object contains size of wrapper and maragins
  let dimensions = {
    width: window.innerWidth * 0.9,
    height: 400,
    margin: {
      top: 15,
      right: 15,
      bottom: 40,
      left: 60,
    },
  }

  dimensions.boundedWidth = dimensions.width
    - dimensions.margin.left
    - dimensions.margin.right
  dimensions.boundedHeight = dimensions.height
    - dimensions.margin.top
    - dimensions.margin.bottom


  //---- 3. Draw Canvas
  // select the element with the id #wrapper 
  const wrapper = d3.select('#wrapper')
                  .append('svg')
                  .attr('width', dimensions.width)
                  .attr('height', dimensions.height);

  // using interpolation to translate into string
  const bounds = wrapper.append('g')
                .style('transform', `translate(${dimensions.margin.left}px,
                  ${dimensions.margin.top}px)`);


  //---- 4. Create Scales
  // To create a new scale we need to create the instace of d3.scaleLinear()
  // d3.extent takes 2 parameters, an array of data points, an accessor function
  // .range is the highest and lowest number we want our scale to output in this case the max and min number of pixels on the x axis
  const yScale = d3.scaleLinear()
                .domain(d3.extent(dataset, yAccessor))
                .range([dimensions.boundedHeight, 0]);

  // The outputted number tells us how far away the freezing point from the bottom of the y axis
  console.log(yScale(32));

  const freezingTemperaturePlacement = yScale(32);

  // Black rectangle spanning width of our bounds
  const freezingTemperatures = bounds.append('rect')
                                .attr('x', 0)
                                .attr('width', dimensions.boundedWidth)
                                .attr('y', freezingTemperaturePlacement)
                                .attr('height', dimensions.boundedHeight
                                  - freezingTemperaturePlacement)
                                .attr('fill', '#e0F3F3');

  // Scale for the x axis 
  const xScale = d3.scaleTime()
                  .domain(d3.extent(dataset, xAccessor))
                  .range([0, dimensions.boundedWidth]);                             

  // d3.line mthod will create a generator that converts data points into a d string
  // Find the x axis and y value
  // Transforms data point with both accessor functions and the scale to get the scaled value in pixel space. 
  const lineGenerator = d3.line()
                        .x(d => xScale(xAccessor(d)))
                        .y(d => yScale(yAccessor(d)));

  // Adds the path element to our bounds
  const line = bounds.append('path')
                .attr('d', lineGenerator(dataset))
                .attr('fill', 'none')
                .attr('stroke', '#AF9358')
                .attr('stroke-width', 2);


  //---- 4. Drawing Axes Labels
  // Labels for y axis to be to the left axis. axisLeft() to pass it to the yScale
  const yAxisGenerator = d3.axisLeft().scale(yScale);

  // Creating a g element that would hold a lot of elements created by axis generator
  // Using .call to prevent saving our selection as variable preserve the selection for additional chaining
  const yAxis = bounds.append('g')
                      .call(yAxisGenerator);

  const xAxisGenerator = d3.axisBottom().scale(xScale);
  const xAxis = bounds.append('g')
                      .call(xAxisGenerator)
                      // translatey is to place the labels in the bottom of the x asis 
                      .style('transform', `translateY(${dimensions.boundedHeight}px)`);
  
}

drawLineChart();